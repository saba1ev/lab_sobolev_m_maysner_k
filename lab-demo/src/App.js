import React, { Component, Fragment } from 'react';
import './App.css';
import Card from './components/Card/Card';
import CardDeck from './components/CardDeck/CardDeck';
import PokerHand from './components/PokerHand/PokerHand';




class App extends Component {
  state = {
    cards: [],
    comb: ''
  };

  suffleCards = () => {
    const cardDeck = new CardDeck();
    const fiveCards = cardDeck.getCards(5);
    this.setState({
      cards: fiveCards
    }, () => this.GetOutCome())

  };
  GetOutCome = () =>{
    const pokerHand = new PokerHand();
    const combination = pokerHand.getOutCome(this.state.cards);
    this.setState({
      comb: combination
    })
  };
  render() {
    return (
       <Fragment>
         <div>
           <div>
             <button onClick={this.suffleCards}>Suffle</button>
           </div>
           <div>
             <Card cards={this.state.cards}/>
           </div>
            <p>{this.state.comb}</p>
         </div>
       </Fragment>
    );
  }
}

export default App;
