class CardDeck {
  constructor() {
    this.suit = ['S', 'D', 'H', 'C'];
     this.rank = [
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      'J',
      'Q',
      'K',
      'A'
    ];
     this.allCards = [];

     for(let i = 0; i < this.rank.length; i++) {
       for(let j = 0; j < this.suit.length; j ++) {
         const card = {rank: this.rank[i], suit: this.suit[j]};
         this.allCards.push(card);
       }
     }
  }

  getCard() {
    const rand = Math.floor(Math.random() * this.allCards.length);
    const randCard = this.allCards[rand];
    this.allCards.splice(rand, 1);
    return randCard;
  }
  getCards(howMany){
    const array = [];
    for (let i = 0; i < howMany; i++){
      array.push(this.getCard());
    }
    return array
  }
}

export default CardDeck;