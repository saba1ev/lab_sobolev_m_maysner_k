import React from 'react';
import './Card.css';

const Card = (props) =>{
  const suits = {
    'H' : '♥',
    'D' : '♦',
    'S' : '♠',
    'C' : '♣'
  };

  return(
    props.cards.map((card, index) => {
      return (
        <div key={index} className={'Card Card-rank-' + card.rank + ' ' + 'Card-' + card.suit}>
          <span className='Card-rank'>{card.rank}</span>
          <span className='Card-suit'>{suits[card.suit]}</span>
        </div>
      )
    })
  )
};

export default Card;